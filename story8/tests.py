from django.test import TestCase, Client
from django.urls import reverse

from json import loads
from . import views, apps

# Create your tests here.


class Story8Tests (TestCase):
    # --------------- Testing URLs ---------------
    def test_url_library_exists_in_story8(self):
        response = self.client.get('/books/')
        self.assertEquals(response.status_code, 200)

    # --------------- Testing Views ---------------
    def test_view_search(self):
        response = Client().get(reverse('story8:data')+"?q=cari", follow=True)
        self.assertIn("items", response.content.decode('utf-8'))
