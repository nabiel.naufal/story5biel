from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('books/', views.story8, name='books'),
    path('data/', views.search, name='data'),
]
