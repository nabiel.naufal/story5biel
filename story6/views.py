from django.shortcuts import render, redirect
from . import forms
from . import models

# Create your views here.


def my_schedule(request):
    if(request.method == "POST"):
        if('agenda' in request.POST):
            form1 = forms.AddActivity(request.POST or None)
            if form1.is_valid():
                model1 = models.Activity()
                model1.agenda = form1.cleaned_data['agenda']

                model1.save()
            return redirect("/my-schedule/")
        elif('person' in request.POST) and ('actvty' in request.POST):
            form2 = forms.AddParticipant(request.POST or None)
            if form2.is_valid():
                model2 = models.Participant()
                model2.person = form2.cleaned_data['person']
                model2.actvty = form2.cleaned_data['actvty']

                model2.save()
            return redirect("/my-schedule/")

    form1 = forms.AddActivity()
    form2 = forms.AddParticipant()
    model1 = models.Activity.objects.all()
    model2 = models.Participant.objects.all()
    context = {
        'activity_form': form1,
        'participant_form': form2,
        'activity': model1,
        'participant': model2,
    }
    return render(request, 'home/my_schedule.html', context)


def delete_activity(request, idx):
    agd = models.Activity.objects.get(id=idx)
    peep = models.Participant.objects.filter(actvty=agd)

    for p in peep:
        p.delete()
    agd.delete()

    return redirect("/my-schedule/")


def delete_participant(request, idx):
    models.Participant.objects.get(id=idx).delete()

    return redirect("/my-schedule/")
