from django import forms

from .models import Activity, Participant

class AddActivity(forms.ModelForm):
    class Meta:
        model = Activity 
        fields = [
            'agenda',
        ]
    agenda = forms.CharField(widget=forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder' : 'Enter an activity',
                'type' : 'text',
                'required' : True,
            }))

class AddParticipant(forms.ModelForm):
    class Meta:
        model = Participant
        fields = [
            'person',
            'actvty',
        ]
    person = forms.CharField(widget=forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder' : 'Enter your name',
                'type' : 'text',
                'required' : True,
            }))

    actvty = forms.ModelChoiceField(
                queryset = Activity.objects.all(),
            )