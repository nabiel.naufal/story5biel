from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('my-schedule/', views.my_schedule, name='schedule'),    
    # path('my-schedule/<int:idx>/add/', views.add_participant, name='addP'),
    path('my-schedule/<int:idx>/delete/', views.delete_activity, name='deleteA'),
    path('my-schedule/<int:idx>/delete/p/', views.delete_participant, name='deleteP'),
]
