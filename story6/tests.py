from django.test import TestCase, Client
from django.http import HttpRequest, request, response
from django.urls import reverse, resolve

from .models import Activity, Participant

from . import views
from . import forms
from . import apps

# Create your tests here.


class Story6Tests (TestCase):
    # --------------- Testing URLs ---------------
    # def test_url_exists_in_story6(self):
    #     response = self.client.get('/my-schedule/')
    #     self.assertEquals(response.status_code, 200)

    # --------------- Testing Models ---------------
    def test_model_add_activity(self):
        # Make one activity
        Activity.objects.create(agenda='Jalan')
        # Check if there's one activity
        num = Activity.objects.all().count()
        self.assertEquals(num, 1)

    def test_model_add_participant(self):
        # Make one object and one participant participating
        Activity.objects.create(agenda='Tidur Siang')
        agd = Activity.objects.get(agenda='Tidur Siang')
        Participant.objects.create(person='Adek', actvty=agd)
        # Check if there's one participant
        num = Participant.objects.all().count()
        self.assertEquals(num, 1)

    def test_model_relationship(self):
        # Make one activity and one participant
        Activity.objects.create(agenda='Makan')
        idx = Activity.objects.all()[0].id
        agd = Activity.objects.get(id=idx)
        # Make participant using
        Participant.objects.create(person='DoRe', actvty=agd)
        Participant.objects.create(person='MiFa', actvty=agd)
        Participant.objects.create(person='SoLa', actvty=agd)
        # Check if there's one participant
        num = Participant.objects.filter(actvty=agd).count()
        self.assertEquals(num, 3)

    def test_model_activity_str(self):
        # Make one activity
        Activity.objects.create(agenda='Makan Ikan')
        idx = Activity.objects.all()[0].id
        agd = Activity.objects.get(id=idx)
        self.assertEquals(str(agd), 'Makan Ikan')

    def test_model_participant_str(self):
        # Make one object and one participant participating
        Activity.objects.create(agenda='Tidur Siang')
        agd = Activity.objects.get(agenda='Tidur Siang')
        Participant.objects.create(person='Adek', actvty=agd)
        idx = Participant.objects.all()[0].id
        cat = Participant.objects.get(id=idx)
        self.assertEquals(str(cat), 'Adek')

# --------------- Testing Views ---------------
    def test_view_delete_activity(self):
        # Make one activity
        Activity.objects.create(agenda='Cry Myself to Sleep')
        idx = Activity.objects.all()[0].id
        # Delete the activity
        response = Client().post('my-schedule/', views.delete_activity('POST', idx=idx))
        # Check if there's no activity
        num = Activity.objects.all().count()
        self.assertEquals(num, 0)

    def test_view_delete_participant(self):
        # Make one object and one participant participating
        Activity.objects.create(agenda='Get Simped')
        agd = Activity.objects.get(agenda='Get Simped')
        Participant.objects.create(person='Cu Chulainn', actvty=agd)
        idx = Participant.objects.get(person='Cu Chulainn').id
        # Delete the activity
        response = Client().post('my-schedule/', views.delete_participant('POST', idx=idx))
        # Check if there's no participant
        num = Participant.objects.all().count()
        self.assertEquals(num, 0)

    def test_view_add_activities_response(self):
        response = self.client.post(
            '/my-schedule/', data={'agenda': 'Go Home'})
        self.assertEquals(response.status_code, 302)

    def test_view_add_activities_counted(self):
        response = self.client.post(
            '/my-schedule/', data={'agenda': 'Go Home'})
        num = Activity.objects.filter(agenda='Go Home').count()
        self.assertEquals(num, 1)

    def test_view_add_participant_response(self):
        Activity.objects.create(agenda='Get Simped')
        agd = Activity.objects.get(agenda='Get Simped')
        response = self.client.post(
            '/my-schedule/', data={'person': 'Gilgamesh', 'actvty': agd})
        self.assertEquals(response.status_code, 302)

    def test_view_add_participant_counted(self):
        Activity.objects.create(agenda='Get Simped')
        agd = Activity.objects.get(agenda='Get Simped').id
        response = self.client.post(
            '/my-schedule/', data={'person': 'Gilgamesh', 'actvty': agd})
        num = Participant.objects.filter(actvty=agd).count()
        self.assertEquals(num, 1)

# --------------- Testing Apps ---------------
    def test_app_story6_1(self):
        appname = apps.Story6Config.name
        self.assertEqual(appname, 'story6')
