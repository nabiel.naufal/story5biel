from django.db import models

# Create your models here.
class Activity(models.Model):
    agenda    = models.CharField(blank=False, max_length=20) #max_lenth = required
    def __str__(self):
        return self.agenda

class Participant(models.Model):
    person  = models.CharField(blank=False, max_length=20) #max_lenth = required
    actvty = models.ForeignKey(Activity, on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.person