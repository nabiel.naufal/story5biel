from django.test import TestCase, Client
from django.urls import reverse

from . import views, apps

# Create your tests here.


class Story8Tests (TestCase):
    # --------------- Testing URLs ---------------
    def test_url_index_exists_in_story9(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)

    def test_url_signup_exists_in_story9(self):
        response = self.client.get('/signin/')
        self.assertEquals(response.status_code, 200)

    # --------------- Testing Views ---------------
    def test_view_signup_successful(self):
        response = self.client.post('/signom/', data={
            'username': 'TestUser',
            'password1': 'tolongtpsusah',
            'password2': 'tolongtpsusah'
        })
        self.assertEquals(response.status_code, 302)
