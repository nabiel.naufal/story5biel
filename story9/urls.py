from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'story9'

urlpatterns = [
    path('index/', views.story9, name='login'),
    path('signin/', views.signin, name='signin'),
]
