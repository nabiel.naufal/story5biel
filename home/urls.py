from django.urls import path, include
from django.contrib import admin
from .views import showpelajaranView, pelajarandetailsView, deletepelajaranView
from . import views
app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),

    path('about/', views.about, name='about'),
    path('experience/', views.experience, name='experience'),
    path('contact/', views.contact, name='contact'),
    path('story1/', views.story1, name='story1'),
    path('formpelajaran/', views.formpelajaran, name='formpelajaran'),
    path('showpelajaran/', showpelajaranView.as_view(), name='showpelajaran'),
    path('pelajarandetails/<int:pk>',
         pelajarandetailsView.as_view(), name='pelajarandetails'),
    path('pelajarandetails/<int:pk>/deletepelajaran',
         deletepelajaranView.as_view(), name='deletepelajaran'),
]
