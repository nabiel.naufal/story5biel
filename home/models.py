from django.db import models
from django.urls import reverse

# Create your models here.


class pelajaran(models.Model):
    namapelajaran = models.CharField(max_length=30, default="Nama pelajaran")
    dosen = models.CharField(max_length=30, default="Mas Toto")
    sks = models.CharField(max_length=10, default="4")
    tahun = models.CharField(max_length=20, default="Semester/Tahun")
    deskripsi = models.CharField(
        max_length=300, default="Deskripsikan matkul tersebut")
    ruang = models.CharField(max_length=10, default="B102")

    def get_absolute_url(self):
        return reverse('home:list')
