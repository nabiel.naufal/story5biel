$("#searchBox").keyup( function() {
    var keyword = $("#searchBox").val();

    $.ajax({
        url : "/data?q=" + keyword,
        success : function(data) {
            var booksArray = data.items;

            $("#table").empty();  // clears existing data
            $("#table").append("<tr style='font-weight: 700; font-size: larger;'><th>Title</th><th>Author(s)</th></tr>");
            for (i = 0; i < booksArray.length; i++) {
                // loads 10 objects' infos
                var cover = booksArray[i].volumeInfo.imageLinks.smallThumbnail;
                var title = booksArray[i].volumeInfo.title;
                var author = booksArray[i].volumeInfo.authors;

                $("#table").append("<tr><th>"+title+ "<br><img src=" +cover+ "></th><th>" +author+ "</th></tr>");
                console.log(title);
            }
        }
        
    });
});
