$('.ext').click(function(e) {
    const accordion = document.getElementsByClassName('contentCard');
    for (i = 0; i < accordion.length; i++) {
        accordion[i].addEventListener('click', function(){
            this.classList.toggle('active')
        })
    }
});

$('.btnDown').click(function() {
    var $this = $(this).parent().parent().parent();
    $this.next().insertBefore($this);
});

$('.btnUp').click(function() {
    var $this = $(this).parent().parent().parent();
    $this.prev().insertAfter($this);
});
